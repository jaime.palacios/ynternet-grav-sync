---
title: 'Copyleft et licences FLOSS'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: fr
length: '180 min'
materials:
    - 'Ordinateur personnel (Smartphone ou tablette connectée à internet)'
    - 'Connexion Internet'
    - Beamer
    - 'Papier et stylos'
    - 'Tableau de conférence'
    - 'Académie Slidewiki'
objectives:
    - 'Les droits d''auteur ont été la cible de critiques, avec le développement des technologies qui facilitent la copie et le partage de l''information'
    - 'Les objectifs du copyleft et du FLOSS sont au service des communautés et des contenus éducatifs'
    - 'Le cadre Creative Commons destiné aux auteurs qui préfèrent partager leur travail et enrichir le patrimoine commun (les Commons) peut être activé'
---

"Tous droits réservés", "Marque", "brevet", "copie ou reproduction limitée à un usage strictement privé" ... Quand on parle de "culture", on est toujours ramené à la notion d'appropriation (propriété), dans ce cas, intellectuel.

Pourtant, la tendance dans la culture libre, c'est que les idées appartiennent à tout le monde et sont, dans une petite mesure comme l'air et l'eau, nos besoins de base. La culture du copyleft, également appelée culture libre, est née du monde du logiciel et des nombreux contributeurs qui avaient un point commun: leur sens du bien commun.

L'expression «logiciel libre» fait référence à la liberté, pas au prix. Pour comprendre le concept, il faut penser à la "liberté d'expression", pas au "libre accès". Inspirées par cette façon novatrice de penser à la façon de gérer la production créative, d'autres initiatives ont progressivement éloigné le copyleft du monde logiciel.

Ce module sera divisé en 3 séances :
###Première séance : Copiez quoi? Compréhension et utilisation des licences FLOSS
Cette session fournira un cadre historique et politique des licences copyleft et FLOSS. Comprendre et pouvoir utiliser les licences FLOSS comme outil pour faire évoluer les questions de propriété, de communauté et de droit d'auteur autour de la société et de la technologie. Les participants pourront identifier les licences copyleft et FLOSS et reconnaître leurs objectifs.

###Deuxième séance : les licences FLOSS dans des scénarios réels sont partout
La deuxième session visera à définir le contexte des licences FLOSS avec des études de cas réelles.Les participants entreprendront une analyse critique de l'utilisation des licences FLOSS dans diverses entités (européennes et internationales) et concevront une politique de licence initiale pour leur propre organisation.

###Troisième séance : licences FLOSS pour l'enseignement ouvert, l'art et l'apprentissage collectif
Cette session permettra une analyse critique de l'importance des licences FLOSS dans le domaine de la formation non formelle. Il continuera en examinant les avantages du copyleft et des licences gratuites dans différents environnements éducatifs et connectera les participants aux mouvements de licence FLOSS existants.