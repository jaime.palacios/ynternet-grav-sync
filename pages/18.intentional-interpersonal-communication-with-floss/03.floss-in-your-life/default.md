---
title: 'FLOSS in your life'
objectives:
    - 'to provide a critical analysis of the importance of FLOSS licences for open education, art and collective learning'
    - 'to examine the use and advantages of FLOSS licences in education and art'
    - 'to foster learners''active and creative engagement through FLOSS licences'
    - 'to motivate participants to adopt FLOSS licences in their current and future education activities'
length: '180 min'
---

##Introduction
Description of the activity
→ Group discussion: Introduction to the session ( theme: licences, education or art)
Participants will brainstorm on licences and education initiatives around them. Answers will be documented and then reused to customize the training material.

##Is FLOSS for you?
A discussion on the potential benefits of using FLOSS include:
* the possibility of developing further your digital skills.
* improved software quality created by the collective power of talented community members.
* the ability to customise software for own usage.
* fair competition in the software-market.
* freedom from vendor-lock in, meaning that there is no more the risk of becoming locked in by the vendor or technology and then be at the mercy of vendors' price increases and experience a lack of flexibility you can't easily and readily escape. 
* the huge potential of increasing security, as it allows for much more thoroughly reviewed independent security checks that help close security holes faster.
* long-term sustainability of software with quicker development and troubleshooting, since there is a powerful community when issues arise or the possibility to pick another provider and potentially influence the development, no vendor lock-in etc.
* increasing stability and protection of privacy, mainly when the source code is public, since this will increase the possibility to detect harmful behaviour, and enable experts to fix it even if the vendor doesn’t want to.
* increasing reliability since more eyes are on FOSS, the source code tends to be superior and the output very robust.
* companies and users have more control over their own hardware devices, own IT systems and associated digital artefacts.
* that global and united communities are improving FOSS by often introducing new concepts and capabilities faster, better, and more effectively.
* decreasing software costs in mid- to long-term as FOSS require no licensing fees.

The business models for FLOSS are:
* Auxiliary services.
* Corporate development and distribution.
* SaaS with distribution of server software.
* Dual Dual-licensing/Selling exceptions licensing.
* Membership and donations.
* Crowdfunding.
* Advertising.
* Update subscriptions.
* User data.
* Software certification.

Contribute your resources
Free and Open Source Software is only possible thanks to its strong communities. These consist of voluntary and non-paid individuals, organisations, companies and public bodies. Because of this variety, there are countless ways how these actors can support FOSS and the particular software projects.

Obviously, there are developers. Many projects would not exist without volunteers who contribute code, translations, testing and design improvements on a non-paid basis in their free time. There are also companies developing FOSS with a multitude of business models. Also non-profit organisations and public bodies develop their own software or improve existing software on the market.
Since FOSS often can be used for free by any user and organisation, FOSS projects depend on financial support. This can happen through donations by individuals, companies or funds. Companies and organisations can also support a software project indirectly by contracting the main developers or external firms to create new features, and publishing these under a FOSS licence in order to merge it upstream to the original project.

##Description of the activity
→ Watch the video on advantages for SMEs (“[What's In It for Me? Benefits from Open Sourcing Code](https://www.youtube.com/watch?v=ZtYJoatnHb8) - Open Source Developers @ Google Speaker Series: Ben Collins-Sussman and Brian Fitzpatrick”, 25.10.2007).
Study, document and discuss its main arguments. Create a common document with all participants, share it with others.

##Homework
Search in the [Zenodo](https://zenodo.org/) open repository and find your three favourite papers on FLOSS and education. Document and/or post a comment on them.

#References