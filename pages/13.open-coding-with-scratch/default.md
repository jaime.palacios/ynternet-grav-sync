---
title: 'Open coding with Scratch'
length: '10 hours'
objectives:
    - 'to code in Scratch for developing interactive stories, games and animation'
    - 'to use Scratch for working collaboratively (share a project from the Scratch Online Editor, view and remix another user’s project)'
    - 'to introduce participants to Scratch Philosophy and learn how to code in Scratch'
    - 'share own projects and use other user’s projects'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - 'Scratch program'
skills:
    Keywords:
        subs:
            Scratch: null
            'open coding': null
            'scratch philosophy': null
            'sequential processing': null
            'code block': null
            blocks: null
            projects: null
            stage: null
            scripts: null
---

Scratch is a free programming language and online community developed by MIT which can be used to create games, animations, songs and share them online.
Scratch is used by people of all ages in a wide variety of settings. The ability to code computer programs is an important part of literacy in today’s society. When people learn to code in Scratch, they learn important strategies for solving problems, designing projects, and communicating ideas.
Statistics on the language's official website show more than 40 million projects shared by over 40 million users, and almost 40 million monthly website visits.

This module will be divided into 3 sessions:
First session – Scratch Philosophy: “Imagine, program, share” 
The philosophy of Scratch encourages the sharing, reuse, and combination of code. This session will provide an overview of the Scratch philosophy "Imagine, Program, Share”.
Users can create their own projects or reuse someone else's project. Projects created and remixed with Scratch are licensed under the Creative Commons Attribution-Share Alike License. 
The session aims to gain greater understanding of Scratch’s background and principles for creating, programming and sharing material. 
At the end of the session, participants will be able to examine the use and advantages of the Scratch Philosophy in adult education.
Second session: Open coding with the free visual programming Scratch 
When people learn to code in Scratch, they learn important strategies for solving problems, designing projects, and communicating ideas. So, coding in Scratch helps users to think creatively, reason systematically and work collaboratively – essential skills for life in the 21st century.
The second session aims at introducing the programming language developed by MIT.
Participants will learn how to code in Scratch for developing interactive stories, games and animation. 
Third session: The effectiveness of Scratch in creating collaborative programming environment
This Module aims at illustrating the fundamentals of the online community 
The Session aims at supporting learners to identify and profit from the Scratch online community. Participants will learn how to share a project from the Scratch Online Editor, view and remix another user’s project getting inspired by others at the same time.
