---
title: 'Promotion and dissemination'
objectives:
    - 'to be aware of how this migration can affect the people participating in each center'
    - 'to set up didactic strategies to work on the new SO'
    - 'to propose strategies for the dissemination of FLOSS'
    - 'to design strategies to adapt the didactic material'
---

##The first impressions
Discussion forum, in order to ask the participants to share their experience and to present the first reactions of the participants in each center.
Building (all together) some answers to balance the difficulties that have shown up. 
On-line activity or on-site discussion groups

##Set of reasons given with the aim of persuading others that the migration is a good idea and answers to the problems 
There are two scenarios:
1. The participants are regular in the center. Did you provide notice previously that a change will be made? How did you did? What did you explained to them?
2. The participants are new to the center. How will you work with this new technology?

The trainee must take note of the most common strategies and summarize them at the end of the session. In case 2, it's important to realize that if the OS is not explained when working with Windows or Mac, it does not makes sense to explain when working with GNU / Linux either.
Face-to-face format: role-playing 
Online format: discussion forums

##The didactic material
Finally, the need to adapt the existing teaching material is addressed, in order to continue the sessions in each center.
It is proposed to re-elaborate the existing material to work on digital competences and not so much on tools. P.e, to talk about office suites I not from MSOffice; to talk about text editors and not about Word...

Each participant is required to propose an activity that is going on in his/her class and to present the supporting material that is being used. The exercise will consist on updating this material according to the new environment and the new perspective. The trainer gives advice and guidance in the  adaptation of the previous proposal.

The delivery of this material will be made in an online and open environment, so that it serves as a learning tool for the other participants.

