---
title: 'FLOSS is everywhere'
length: '195 minutes'
objectives:
    - 'to provide a state of the art around FLOSS'
    - 'to analyse FLOSS initiatives in EU countries'
    - 'to discover the FLOSS the policy framework in EU'
    - 'to motivate educated policy contribution in the FLOSS area'
---

The second session will aim at setting the state of the art around FLOSS. FLOSS practices in EU countries will be presented and compared. Participants will engage in a critical analysis of the policy framework of FLOSS in EU and design an initial policy for their own organisation.

##1.- Introduction
Trainer will start the introduction to the module by asking participants about the FLOSS initiatives around them - mostly coming from other organisations or the EU. Answers will be documented and then reused to customize the training material.

##2.- FLOSS in EU and elsewhere
FLOSS culture will be linked to Commons oriented examples under the following areas:
agriculture
manufacturing
medicine and health
housing construction
circular economy
urban development
water management
crypto-programming
disaster response
knowledge
communication
IT Infrastructure

##3.- An EU policy framework for FLOSS
Presentation and analysis of the main EU FLOSS policy initiatives including:
* [Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#softwarestrategy)
* [Open data policy](http://data.europa.eu/euodp/en/data/)
* [Open standards](https://ec.europa.eu/digital-single-market/en/open-standards)
* [The European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12) (the ‘EUPL’)

##4.- Your policy framework for FLOSS
Join a group to design and share your policy proposal for FLOSS both nationally and EU level. What should be people doing?

##Homework
Wikify your policy framework. Use a platform of your choice to share and discuss your policy ideas on FLOSS.

##References
[FLOSS as Commons](http://www.bollier.org/floss-roadmap-2010-0). David Bollier, Floss Road Map, 2010.