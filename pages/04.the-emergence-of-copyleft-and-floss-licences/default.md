---
title: 'The emergence of copyleft and FLOSS licences'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: en
length: '10 hours'
materials:
    - 'Personal computer (Smartphone or tablet connected to the Internet)'
    - 'Internet connexion'
    - Beamer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
objectives:
    - 'Copyright and author''s rights have been the target of criticism, with the development of technologies that facilitate information copying and sharing'
    - 'The goals of copyleft and FLOSS serve educational communities and content'
    - 'The Creative Commons framework aimed at authors who prefer to share their work and enrich the common heritage (the Commons) can be activated'
---

"All rights reserved", "Trademark", "patent", "copying or reproduction limited to strictly private use" ... When we talk about "culture", we are always brought back to the notion of appropriation (property), in this case, intellectual. Yet, the trend in free culture, is that ideas belong to everyone, and are, to a small extent like the air and water, our basic needs. The copyleft culture, also called free culture, was born from the world of software and the many contributors who had one thing in common: their sense of the common good. 
The expression "free software" refers to freedom, not price. To understand the concept, you have to think of "freedom of expression", not "free access". Inspired by this innovative way of thinking about how to handle creative output, other initiatives have gradually moved copyleft out of the software world.

This module will be divided into 3 sessions:
First session - Copywhat ? Understanding and use of FLOSS licences 
This session will provide a historical and policy framework of copyleft and FLOSS licences. Understanding and being able to put in use FLOSS  licences as a tool for evolving property, community and copyright questions around society and technology. Participants will be able to identify copyleft and FLOSS  licences and recognise their goals.

Second session: FLOSS licences in real life scenarios is everywhere
The second session will aim at setting the context around FLOSS licences with actual case studies Participants will engage in a critical analysis of the use of FLOSS licences in various entities (EU and international) and design an initial licence policy for their own organisation.

Third session: FLOSS licences for open education, art and collective learning
This session will allow for a critical analysis of the importance of FLOSS licences  in the field of non-formal training. It will continue by examining the advantages of copyleft and free licences in different educational environments and connect participants with existing FLOSS licencing movements.
