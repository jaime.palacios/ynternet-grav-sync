---
title: 'Appliquer WikiData'
length: '120 min'
objectives:
    - 'identifier la valeur appliquée de Wikidata, en particulier pour des applications comme la visualisation de données complexes'
    - 'identifier des applications potentielles pour Wikidata dans le cadre de leur propre travail, que ce soit par la visualisation, l''intégration de Wikidata dans leur propre environnement logiciel'
    - 'savoir comment contacter et se connecter avec les membres de la communauté Wikidata'
---

##1.- Questions sur le dernier module et discussion sur les travaux à la maison (formateur) (20 min)
Feedback sur sparql et requêtes.
Résultats de la campagne ISA et mise en évidence de la relation avec le module du jour (utilisation des informations wikidata pour améliorer la description des images avec des données structurées).

##2.- (Potentiel) valeur appliquée de Wikidata (formateur) (90 min)
→ Aperçu et présentations (45 min)
Cette section peut être divisée en 3 parties. 
* La première pourrait fournir un aperçu du contenu déjà disponible sur WikiData : chiffres et statistiques par thème.
* La seconde pourrait fournir des exemples d'utilisation de Wikidata par les projets de Wikimedia.
Exemples suggérés : (à choisir en fonction de l'auditoire)
1. un outil pour explorer l'écart entre les sexes dans le contenu de Wikipédia, comme Denelezh.
2. l'utilisation du bot Listeria pour créer des RedLists d'articles manquants. Voir Category:Lists_based_on_Wikidata.
3. l'utilisation de Wikidata pour ajouter des avis d'autorité sur les articles de Wikipédia. Exemple de Doris Day.
4. l'utilisation de Wikidata pour tenir à jour les infoboxes sur Wikipedia. Exemple de Jack Spicer.
5. l'utilisation de la fonction d'analyseur pour afficher directement les informations de WikiData dans les articles (voir how_to_use_data_on_Wikimedia_projects).
6. l'utilisation d'infoboxes logiques sur Wikipédia (comme l'utilisation de {{infobox fromage}}} ou {{infobox biographies2}}} sur Wikipédia français).
* Le troisième fournirait des exemples d'utilisation de Wikidata par des tiers.
Exemples d'exemples à présenter (à choisir en fonction de l'auditoire) :
1. divers outils de visualisation tels que : Le Wikidata Graph Builder ou Histropedia (application de timeline basée sur Wikidata)
2. crotos, un moteur de recherche et d'affichage d'œuvres d'art visuel basé sur Wikidata et utilisant les fichiers Wikimedia Commons
3. Scholia : un moteur de recherche et d'affichage pour les universitaires basé sur Wikidata
Lire :
* [Denelezh](https://www.denelezh.org)
* [Lists based on Wikidata](https://en.wikipedia.org/wiki/Category:Lists_based_on_Wikidata)
* [Doris Day](https://en.wikipedia.org/wiki/Doris_Day)
* [Jack Spicer](https://en.wikipedia.org/wiki/Jack_Spicer)
* [How to use data on Wikimedia projects](https://www.wikidata.org/wiki/Wikidata:How_to_use_data_on_Wikimedia_projects)
* [Showcases](https://phabricator.wikimedia.org/T168057)
* [Wikidata knowledge as a service](https://www.wikidata.org/wiki/File:Wikidata_Knowledge_as_a_Service_slides_OeRC_Feb2018.pdf)
* [Histropedia](http://histropedia.com)
* [Angryloki](https://angryloki.github.io/wikidata-graph-builder/?property=P279&item=Q5)
* [Crotos](http://zone47.com/crotos/)
* [Sholia](https://tools.wmflabs.org/scholia/)

→ Activité (sur ordinateur portable, 45 min)
Fournir aux participants la liste des outils sélectionnés et les laisser explorer

##3.- Connexion avec la communauté WikiData (formateur) (45 min)
→ Comment se connecter (45 min)
Le but de cette section est de savoir comment contacter et se connecter aux membres de la communauté Wikidata. Comment rester en contact avec les dernières nouvelles du projet et où trouver des ressources.
Passez en revue les options à discuter avec un wikidatien sur une page de discussion.
Présenter les WikiProjets et expliquer comment les rejoindre ou interagir avec ses membres. Expliquer les avantages de ces WikiProjets, mais aussi leur gouvernance (liberté d'adhésion, etc.). Idéalement, choisissez de montrer un Wikiprojet pertinent pour le public.
Suggérez de rejoindre la liste de diffusion wikidata, les listes de diffusion techniques, le canal IRC, etc... tous disponibles sur la page principale WikiData. Interagir avec l'auditoire pour voir ce qui serait le plus approprié pour lui.
Un point d'entrée intéressant pour montrer comment la communauté interagit est d'afficher la page de demande de commentaires.
Suggérez un contact avec la section locale s'il y en a une dans le pays, et avec le groupe d'utilisateurs de la communauté wikidata.
Suivez la conférence annuelle de Wikidata, naviguez dans le programme et les présentations fournies (au moins des diapositives, parfois des documents). Le plus généralement les événements.
Lire :
* [WikiData WikiProjects](https://dashboard.wikiedu.org/training/wikidata-professional/wikidata-wikiprojects)
* [Requests for comment](https://www.wikidata.org/wiki/Wikidata:Requests_for_comment)
* [WikiDataCon 2019](https://www.wikidata.org/wiki/Wikidata:WikidataCon_2019)
* [WikiData Events](https://www.wikidata.org/wiki/Wikidata:Events)
* [WikiData movement affiliates](https://meta.wikimedia.org/wiki/Wikimedia_movement_affiliates)
* [WikiData Community User Group](https://meta.wikimedia.org/wiki/Wikidata_Community_User_Group)

##4.- Travail à faire à la maison (60 min)
Search in the https://zenodo.org/ open repository and find your three favourite papers on FLOSS and education. Document and/or post a comment on them.

##5.- Récapitualtion (10 min)
Pour conclure le module, le formateur animera un moment de récapitulation où les participants sont encouragés à exprimer leurs questions, leurs doutes, leurs idées et leurs sentiments sur les sujets abordés.

##Références pour le formateur
*https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
*https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
*https://www.wikidata.org/wiki/Help:Contents
*https://www.wikidata.org/wiki/Wikidata:Tools
*https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources