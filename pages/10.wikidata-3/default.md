---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: it
length: '10 ore'
objectives:
    - 'A solid contextual background to understand the benefits of Wikidata and how it relates to the other Wikimedia projects but also how it relates to other databases available on the Internet'
    - 'The participant will acquire digital skills in being able to add content to Wikidata but also in being able to write queries in Sparql to get answers on difficult questions'
    - 'The participant will be be able to present Wikidata to others and will have enough background to identify how it could be used in professional background'
    - 'The participant will better understand which quality processes and evaluating systems favor the constitution of quality data that every person and every machine can use'
materials:
    - 'Ordinateur personnel connecté à internet'
    - 'Connexion Internet'
    - Beamer
    - 'Papier et stylos'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            WikiData: null
            Database: null
            'Knowledge Graph': null
            Wikipedia: null
            Community: null
            CC0: null
            'Free licences': null
            Collaboration: null
            SPARQL: null
---

WikiData è una piattaforma di dati aperta che appartiene alla famiglia di siti Web Wikimedia. Ospita 57 milioni di articoli a giugno 2019. Wikidata è una base di conoscenza libera e aperta che contiene vari tipi di dati (ad es. Testo, immagini, quantità, coordinate, forme geografiche, date ...).
L'entità di base in Wikidata è un oggetto. Un oggetto può essere una cosa, un luogo, una persona, un'idea o qualsiasi altra cosa. Ciò che è specifico di Wikidata è che le informazioni sono memorizzate in modo rigidamente strutturato per consentire l'elaborazione sia a persone che a macchine.
Sebbene ora sia una base di conoscenza abbastanza ampiamente utilizzata dalle macchine, è ancora ampiamente sconosciuta al pubblico e ai suoi potenziali istruttori, meno compresi. L'intento di questo modulo è quello di fornire un corso Train the Trainer che fornirà loro una solida panoramica di Wikidata e delle opportunità che sono collegate a questo progetto open source.

This module will be divided into 4 sessions:
####First session – Discovering and understanding WikiData
This session will provide an overview of what Wikidata is. It will cover its history and allow the participants to understand the why and the benefits of its existence, for the wikimedia projects in particular, and for the Internet in general. The session will cover Wikidata basics, data structure, elements of vocabulary, and essential design features, but also wikidata community and governance overview so that participants capture the fact wikidata, as technical as it seems, is primarily a social construction. Participants will explore Wikidata by themselves and create an account.
####Second session: Editing WikiData
The second session will aim to get participants to add content to Wikidata on their own and thus discover some of the editing fundamental processes. It will also take a deep dive into data quality and data evaluation systems, as well as discover most popular tools that allow to fill the gaps in depth and wide, as well as tools for mass editing and import.
####Third session: Querying WikiData
This session is meant to get participants discover and experiment the query service for simple queries, then discover sparql querying language for more complex in depth requests through a step-by-step methodology, incrementally increasing the difficulty. The session will include hands-on activities.
####Fourth session: Applying WikiData
This session will help participants to understand the applied value of Wikidata, particularly for applications like visualization of complex data, but also to identify potential applications for Wikidata in their own work, whether through visualization, embedding Wikidata in their own software, or using Wikidata on Wikimedia projects. The second part of the session will teach them how to connect with the Wikidata community for a more effective collaboration.