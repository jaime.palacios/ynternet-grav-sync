---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: en
length: '10 hours'
objectives:
    - 'A solid contextual background to understand the benefits of Wikidata and how it relates to the other Wikimedia projects but also how it relates to other databases available on the Internet'
    - 'The participant will acquire digital skills in being able to add content to Wikidata but also in being able to write queries in Sparql to get answers on difficult questions'
    - 'The participant will be be able to present Wikidata to others and will have enough background to identify how it could be used in professional background'
    - 'The participant will better understand which quality processes and evaluating systems favor the constitution of quality data that every person and every machine can use'
materials:
    - 'PC with internet connexion'
    - 'Internet connexion'
    - Beamer
    - 'Paper et pen'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            WikiData: null
            Database: null
            'Knowledge Graph': null
            Wikipedia: null
            Community: null
            CC0: null
            'Free licences': null
            Collaboration: null
            SPARQL: null
---

WikiData is an open data platform that belongs to the Wikimedia family of websites. It hosts 57 millions items as of June 2019. Wikidata is a free and open knowledge base that contains various data types (eg, text, images, quantities, coordinates, geographical shapes, dates...).
The basic entity in Wikidata is an Item. An item can be a thing, a place, a person, an idea, or anything else. What’s specific to Wikidata is that the information is stored in a rigidly structured way to makes it possible for both humans and machines to process.
Although it is now a knowledge base fairly widely used by machines, it is still largely unknown from the public as well as from its potential instructors, least understood. The intent of this module is to provide a Train the Trainer course that will provide them with a solid overview of Wikidata and opportunities that are attached to this open source project.
